#ifndef LISTA_DWUKIERUNKOWA_H
#define LISTA_DWUKIERUNKOWA_H

class Lista_dwukierunkowa {
    public:
        Lista_dwukierunkowa();                                                          // konstruktor listy dwukierunkowej
        ~Lista_dwukierunkowa();                                                         // destruktor listy dwukierunkowej

        struct ElementListy {                                                           // struktura reprezentujaca element listy (krawedz)
            public:
                ElementListy();                                                         // konstruktor krawedzi
                ~ElementListy();                                                        // destruktor krawedzi

                ElementListy* getNext() { return next; }                                // zwraca wskaznik na NASTEPNA krawedz
                ElementListy* getPrev() { return prev; }                                // zwraca wskaznik na POPRZEDNIA krawedz
                int getValue() { return value; }                                        // zwraca wage krawedzi
                int getV1() { return v1; }                                              // wierzcholek na jednym koncu krawedzi (nieistotne ktory)
                int getV2() { return v2; }                                              // wierzcholek na drugim koncu krawedzi
                void setNext(ElementListy* next) { this -> next = next; }               // ustawia wskaznik na NASTEPNY element
                void setPrev(ElementListy* prev) { this -> prev = prev; }               // ustawia wskaznik na POPRZEDNI element
                void setValue(int value) { this -> value = value; }                     // ustawia wage krawedzi
                void setV1(int v1) { this -> v1 = v1; }                                 // ustawia indeks jednego z wierzcholkow
                void setV2(int v2) { this -> v2 = v2; }                                 // ustawia indeks drugiego wierzcholka
            private:
                ElementListy* next;                                                     // wskaznik na nastepna krawedz
                ElementListy* prev;                                                     // wskaznik na poprzednia krawedz
                int value;                                                              // waga krawedzi
                int v1, v2;                                                             // indeksy wierzcholkow polaczonych krawedzia
        };

        struct Pomoc {                                                                  // struktura pomocnicza przechowujaca wielkosc listy oraz wskaznik na element pierwszy i ostatni
            public:
                Pomoc();                                                                // konstruktor struktury pomocniczej
                ~Pomoc();                                                               // destruktor struktury pomocniczej

                ElementListy* getFirst() { return first; }                              // zwraca wskaznik na PIERWSZY element listy
                ElementListy* getLast() { return last; }                                // zwraca wskaznik na OSTATNI element listy
                int getAmount() { return amount; }                                      // zwraca wielkosc listy
                void setFirst(ElementListy* first) { this -> first = first; }           // ustawia wskaznik na PIERWSZY elementu
                void setLast(ElementListy* last) { this -> last = last; }               // ustawia wskaznik na POPRZEDNI element
                void setAmount(int amount) { this -> amount = amount; }                 // ustawia wielkosc listy
            private:
                ElementListy* first;                                                    // wskaznik na pierwszy element
                ElementListy* last;                                                     // wskaznik na ostatni element
                int amount;                                                             // wielkosc listy dwukierunkowej
        };

        void addToBegin(int, int, int);                                                 // metoda dodajaca nowy element na poczatek
        void addToEnd(int, int, int);                                                   // metoda dodajaca nowy element na koniec
        void addAnywhere(int, int, int, int);                                           // metoda dodajaca nowy element na zadanej pozycji
        void display();                                                                 // metoda wyswietlajaca wszystkie elementy listy
        Pomoc* pomoc;                                                                   // wskaznik na POMOC
        ElementListy* pomocniczy;                                                       // wskaznik pomocny przy dodawaniu krawedzi do listy

    private:
        ElementListy* element;                                                          // wskaznik na pierwszy element listy
        void addFirstElem(int, int, int);                                               // metoda pomocnicza dodajaca pierwszy element do listy ktora domyslnie jest pusta
};

#endif // LISTA_DWUKIERUNKOWA_H
