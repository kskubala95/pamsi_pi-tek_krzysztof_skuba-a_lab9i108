#include "lista_dwukierunkowa.h"
#include <iostream>
#include <iomanip>

// -------------- KONSTRUKTORY I DESTRUKTORY ---------------------------------------------------------------

// Konstruktor listy dwukierunkowej
// W momencie utworzenia listy powstaje element pomocniczy przechowujacy element pierwszy, ostatni oraz wielkosc listy
Lista_dwukierunkowa::Lista_dwukierunkowa() {
    pomoc = new Pomoc();
    pomoc -> setFirst(NULL);
    pomoc -> setLast(NULL);
    pomoc -> setAmount(0);
}

// Destruktor listy dwukierunkowej
// W momencie usuniecia listy usuniety zostaje takze element pomocniczy
Lista_dwukierunkowa::~Lista_dwukierunkowa() { delete pomoc; }

Lista_dwukierunkowa::Pomoc::Pomoc() { }
Lista_dwukierunkowa::Pomoc::~Pomoc() { }
Lista_dwukierunkowa::ElementListy::ElementListy() { }
Lista_dwukierunkowa::ElementListy::~ElementListy() { }

// -------------- METODY PRYWATNE --------------------------------------------------------------------------

// Metoda dodajaca pierwszy element do listy
// Standardowe utworzenie pierwszego elementu poprzedzone jest ustawieniem jego wskaznikow na elementy
// poprzedni i nastepny wartoscia NULL oraz ustawieniem jego wartosci "value"
// Nastepnie element ten staje sie jednoczesnie elementem pierwszym i ostatnim
void Lista_dwukierunkowa::addFirstElem(int value, int v1, int v2) {
    element = new ElementListy();
    element -> setPrev(NULL);
    element -> setNext(NULL);
    element -> setValue(value);
    element -> setV1(v1);
    element -> setV2(v2);
    pomoc -> setFirst(element);
    pomoc -> setLast(element);
    pomoc -> setAmount(1);
}

// -------------- METODY PUBLICZNE -------------------------------------------------------------------------

// Metoda dodajaca element na poczatek listy dwukierunkowej
// Jesli lista nie zawiera elementow zostaje wywolana metoda addFirstElem(int) i metoda konczy dzialanie
// W przeciwnym razie tworzymy nowy element, odpowiednio modyfikujemy wskazniki na elementy poprzednie i
// nastepne jego oraz elementu pierwszego (na ktory wskazuje "element"); po zmodyfikowaniu zawartosci pomocy
// ustawiamy wskaznik "element" na nowo utworzony element, aby mozliwe byly dalsze prawidlowe operacje na liscie
void Lista_dwukierunkowa::addToBegin(int value, int v1, int v2) {
    if(pomoc -> getAmount() == 0) {
        addFirstElem(value, v1, v2);
        return;
    }

    ElementListy* nowy = new ElementListy();
    element -> setPrev(nowy);
    nowy -> setPrev(NULL);
    nowy -> setNext(pomoc -> getFirst());
    nowy -> setValue(value);
    nowy -> setV1(v1);
    nowy -> setV2(v2);
    pomoc -> setFirst(nowy);
    pomoc -> setAmount(pomoc -> getAmount() + 1);
    element = nowy;
}

// Metoda dodajaca element na koniec listy dwukierunkowej
// Dziala niemalze tak samo jak addToBegin(int) z tym, ze wskazniki poszczegolnych elementow ustawiane sa inaczej
void Lista_dwukierunkowa::addToEnd(int value, int v1, int v2) {
    if(pomoc -> getAmount() == 0) {
        addFirstElem(value, v1, v2);
        return;
    }

    ElementListy* nowy = new ElementListy();
    (pomoc -> getLast()) -> setNext(nowy);
    nowy -> setPrev(pomoc -> getLast());
    nowy -> setNext(NULL);
    nowy -> setValue(value);
    nowy -> setV1(v1);
    nowy -> setV2(v2);
    pomoc -> setLast(nowy);
    pomoc -> setAmount(pomoc -> getAmount() + 1);
}

// Metoda dodajaca element na zadanej pozycji listy dwukierunkowej
// Jesli lista nie zawiera elementow zostaje wywolana metoda addFirstElem(int) i metoda konczy dzialanie
// Jesli zadana pozycja wynosi 1 to dodajemy element na poczatku - wywolujemy addToBegin(int) i konczymy
// Jesli zadana pozycja jest wieksza od aktualnej wielkosci listy to dodaje element na koncu poprzez addToEnd(int) i konczy
// Jesli zadana pozycja jest mniejsza od 1 to dodajemy element na poczatku - wywolujemy addToBegin(int) i konczymy
// W przeciwnym razie tworzymy nowy element oraz wskaznik "temp" ktory bedzie pelnil role wskaznika na elementy po ktorych dostaniemy
// sie do pozycji na ktorej chcemy dodac nowy element
// Standardowa procedura dodania nowego elementu poprzedzona jest warunkiem sprawdzajacym czy zadana pozycja jest blizej poczatku
// czy konca listy i na tej zasadzie podejmuje decyzje od ktorej strony sie do niej dostanie (przyspieszanie dodawania)
void Lista_dwukierunkowa::addAnywhere(int value, int v1, int v2, int position) {
    if(pomoc -> getAmount() == 0) {
        addFirstElem(value, v1, v2);
        return;
    }

    if(position == 1) {
        addToBegin(value, v1, v2);
        return;
    }

    if(position > pomoc -> getAmount()) {
        //std::cout << "Wyszedles poza rozmiar listy. Element dodano na koncu." << std::endl;
        addToEnd(value, v1, v2);
        return;
    }

    if(position < 1) {
        //std::cout << "Pozycje oznaczamy od 1. Element dodano na poczatku." << std::endl;
        addToBegin(value, v1, v2);
        return;
    }

    ElementListy* nowy = new ElementListy();
    ElementListy* temp = NULL;

    if((pomoc -> getAmount()) / 2 > position) {
        temp = pomoc -> getFirst();
        for(int i = 1; i < position; i++) temp = temp -> getNext();
    } else {
        temp = pomoc -> getLast();
        for(int i = pomoc -> getAmount(); i > position; i--) temp = temp -> getPrev();
    }

    nowy -> setPrev(temp -> getPrev());
    nowy -> setNext(temp);
    (temp -> getPrev()) -> setNext(nowy);
    temp -> setPrev(nowy);
    nowy -> setValue(value);
    nowy -> setV1(v1);
    nowy -> setV2(v2);
    pomoc -> setAmount(pomoc -> getAmount() + 1);
}

// Metoda wyswietlajaca adres biezacego, poprzedniego oraz nastepnego elementu, a takze wartosc biezacego elementu
void Lista_dwukierunkowa::display() {
    if(element == NULL) { return; }
    std::cout << std::endl;

    ElementListy* temp = pomoc -> getFirst();
    while(temp != NULL) {
        std::cout << std::setw(3) << temp -> getV1() << std::setw(3) << " --- " << temp -> getV2() << std::setw(3) << ", waga: " << temp -> getValue() << std::endl;
        temp = temp -> getNext();
    }
}
