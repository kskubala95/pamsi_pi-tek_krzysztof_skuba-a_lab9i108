#include "graf.h"

// KONSTRUKTORY I DESTRUKTORY ================================================================================
Graf::Graf() {
    macierzSasiedztwa = new grafMacierzowo();
    listySasiadow = new grafListowo();
}
Graf::~Graf() {
    delete macierzSasiedztwa;
    delete listySasiadow;
}

Graf::grafMacierzowo::grafMacierzowo():V(0), E(0), poczatek(0), koniec(0) { }
Graf::grafMacierzowo::~grafMacierzowo() {
    for(int i = 0; i < getV(); i++) delete[] G[i];
    delete[] G;
}

Graf::grafListowo::grafListowo():V(0), E(0), poczatek(0), koniec(0) { }
Graf::grafListowo::~grafListowo() {
    for(int i = 0; i < getV(); i++) {
        nowy_wierzcholek = wierzcholki[i];
        while(nowy_wierzcholek) {
            usuwany_wierzcholek = nowy_wierzcholek;
            nowy_wierzcholek = nowy_wierzcholek -> getNastepny();
            delete usuwany_wierzcholek;
        }
    }

    delete [] wierzcholki;
}
//============================================================================================================

// WYSWIETLANIE POSTACI MACIERZOWEJ GRAFU ====================================================================
void Graf::grafMacierzowo::wyswietl() {
    // Jesli graf nie zawiera wierzcholkow
    if(getV() == 0) {
        cout << "Nie ma czego wyswietlic." << endl;
        return;
    }

    cout << "Postac macierzowa grafu: " << endl << endl;
    // Wyswietlenie wagi krawedzi wychodzacej z wierzcholka 'i' do wierzcholka 'j'
    for(int i = 0; i < getV(); i++) {
        for(int j = 0; j < getV(); j++) {
            cout << setw(4) << G[i][j] << " ";
        }
        cout << endl << endl;
    }
}
//============================================================================================================

// WYSWIETLANIE POSTACI LISTOWEJ GRAFU =======================================================================
void Graf::grafListowo::wyswietl() {
    if(getV() == 0) {
        cout << "Nie ma czego wyswietlic." << endl;
        return;
    }

    cout << "Postac listowa grafu: " << endl << endl;
    for(int i = 0; i < getV(); i++) {
        cout << "V[" << i << "] ---> ";
        nowy_wierzcholek = wierzcholki[i];
        while(nowy_wierzcholek) {
            cout << nowy_wierzcholek -> getId() << "[" << nowy_wierzcholek -> getWaga() << "]";
            nowy_wierzcholek = nowy_wierzcholek -> getNastepny();
            if(nowy_wierzcholek != NULL) cout << ", ";
        }
        cout << endl;
    }
    cout << endl;
}
//============================================================================================================
