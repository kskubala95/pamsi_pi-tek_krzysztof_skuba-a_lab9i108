#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <stdlib.h>
#include <iomanip>
using namespace std;

class Graf {
    public:
        Graf();
        ~Graf();

        class grafMacierzowo {                                                          // Klasa bedaca macierzowa reprezentacja grafu
            private:
                int V, E;                                                               // V - ilosc wierzcholkow, E - ilosc krawedzi
                int poczatek, koniec;                                                   // poczatek - wierzcholek poczatkowy, koniec - wierzcholek koncowy

            public:
                grafMacierzowo();
                ~grafMacierzowo();
                int** G;                                                                // Tablica wskaznikow przechowujaca wagi krawedzi miedzy wierzcholkami

                void setV(int V) { this -> V = V; };
                void setE(int E) { this -> E = E; };
                void setPoczatek(int poczatek) {this -> poczatek = poczatek; }
                void setKoniec(int koniec) { this -> koniec = koniec; }
                int getV() { return V; }
                int getE() { return E; }
                int getPoczatek() { return poczatek; }
                int getKoniec() { return koniec; }

                void wyswietl();                                                        // Funkcja wyswietlajaca graf w postaci macierzowej
                void wczytaj(string);                                                   // Funkcja wczytujaca graf z pliku
                void algorytmDijkstry();
                void algorytmBellmanaForda();
        };

        class grafListowo {                                                             // Klasa bedaca listowa reprezentacja grafu
            private:
                int V, E;                                                               // V - ilosc wierzcholkow, E - ilosc krawedzi
                int poczatek, koniec;                                                   // poczatek - wierzcholek poczatkowy, koniec - wierzcholek koncowy

            public:
                grafListowo();
                ~grafListowo();

                class elementListy {                                                    // Klasa bedaca reprezentacja krawedzi miedzy wierzcholkami
                    private:
                        elementListy* nastepny;                                         // Wskaznik na kolejnego sasiada listy
                        int waga, id;                                                   // waga - koszt przejscia przez krawedz, id - numer wierzcholka od ktorego wychodzi krawedz
                    public:
                        elementListy() { };
                        ~elementListy() { };

                        void setNastepny(elementListy* nastepny) { this -> nastepny = nastepny; }
                        void setWaga(int waga) { this -> waga = waga; }
                        void setId(int id) { this -> id = id; }
                        elementListy* getNastepny() { return nastepny; }
                        int getWaga() { return waga; }
                        int getId() { return id; }
                };

                elementListy** wierzcholki;                                             // Polaczone listy sasiadow (wierzcholkow)
                elementListy* nowy_wierzcholek;                                         // Wskaznik pomocny przy dodawaniu nowego sasiada
                elementListy* usuwany_wierzcholek;                                      // Wskaznik pomocny przy usuwaniu sasiadow

                void setV(int V) { this -> V = V; };
                void setE(int E) { this -> E = E; };
                void setPoczatek(int poczatek) {this -> poczatek = poczatek; }
                void setKoniec(int koniec) { this -> koniec = koniec; }
                int getV() { return V; }
                int getE() { return E; }
                int getPoczatek() { return poczatek; }
                int getKoniec() { return koniec; }

                void wyswietl();                                                        // Funkcja wyswietlajaca graf w postaci listowej
                void wczytaj(string);                                                   // Funkcja wczytujaca graf z pliku
                void algorytmDijkstry();
                void algorytmBellmanaForda();
        };

        grafMacierzowo* macierzSasiedztwa;
        grafListowo* listySasiadow;

        void wygeneruj(bool, bool);                                                           // Funkcja generujaca identyczny graf do postaci macierzowej oraz listowej
        void wczytaj(string);                                                           // Funkcja wczytujaca graf z pliku do postaci macierzowej oraz listowej
        void algorytmDijkstry(bool);
        void algorytmBellmanaForda(bool);
};
