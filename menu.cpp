#include "menu.h"
using namespace std;

Menu::Menu():wybor_operacji(0), nazwa_pliku("") {
    graf = new Graf();
    menu_glowne();
}
Menu::~Menu() { delete[] graf; }

void Menu::operacje_dla_najkrotszej_sciezki() {
    cout << "Ktora operacje chcesz wykonac?"                    << endl;
    cout << "1. Wczytaj graf z pliku"                           << endl;
    cout << "2. Wygeneruj graf losowo"                          << endl;
    cout << "3. Wyswietl graf macierzowo"                       << endl;
    cout << "4. Wyswietl graf listowo"                          << endl;
    cout << "5. Algorytm Dijkstry"                              << endl;
    cout << "6. Algorytm Bellmana-Forda"                        << endl;
    cout << "7. Powrot do menu"                                 << endl;
    cout << endl << "---> Wprowadz wybor: ";
}

void Menu::menu_glowne() {
    cout << "::::::::::::::::::::::::::::::::::::::::::::::::::" << endl;
    cout << "::                                              ::" << endl;
    cout << "::              NAJKROTSZA SCIEZKA              ::" << endl;
    cout << "::                                              ::" << endl;
    cout << "::::::::::::::::::::::::::::::::::::::::::::::::::" << endl;
    najkrotsza_sciezka();
    menu_glowne();

}
void Menu::najkrotsza_sciezka() {
    operacje_dla_najkrotszej_sciezki();
    cin >> wybor_operacji;

    switch(wybor_operacji) {
        case 1:
        {
            cout << "Podaj nazwe pliku: ";
            cin >> nazwa_pliku;
            graf -> wczytaj(nazwa_pliku);
            najkrotsza_sciezka();
        }
        break;
        case 2: /* Wygeneruj graf skierowany */
        {
            cout << "Z krawedziami o wagach ujemnych [1] czy bez [0]: ";
            cin >> decyzja;
            graf -> wygeneruj(1, decyzja);
            najkrotsza_sciezka();
        }
        break;
        case 3: /* Wyswietl graf w postaci macierzy sasiedztwa */
        {
            graf -> macierzSasiedztwa -> wyswietl();
            najkrotsza_sciezka();
        }
        break;
        case 4: /* Wyswietl graf w postaci listy sasiadow */
        {
            graf -> listySasiadow -> wyswietl();
            najkrotsza_sciezka();
        }
        break;
        case 5: /* Algorytm Dijkstry */
        {
            cout << "Dla reprezentacji listowej [0] czy macierzowej [1]: ";
            cin >> wybor_operacji;
            graf -> algorytmDijkstry(wybor_operacji);
            najkrotsza_sciezka();
        }
        break;
        case 6: /* Algorytm Bellmana - Forda */
        {
            cout << "Dla reprezentacji listowej [0] czy macierzowej [1]: ";
            cin >> wybor_operacji;
            graf -> algorytmBellmanaForda(wybor_operacji);
            najkrotsza_sciezka();
        }
        break;
        case 7: /* Wroc do menu glownego */
        {
            menu_glowne();
        }
        break;
        default: /* Pomylka przy wybieraniu opcji */
        {
            cout << endl << "Nigdy nie bylo takiej opcji!" << endl;
            najkrotsza_sciezka();
        }
        break;
    }
}
