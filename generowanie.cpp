#include "graf.h"

void Graf::wygeneruj(bool skierowany, bool decyzja) {
    int ilosc_wierzcholkow, gestosc, krawedzie, maksymalne_ilosc_krawedzi;
    cout << "Podaj liczbe wierzcholkow: ";
    cin >> ilosc_wierzcholkow;

    // Jezeli podalismy za malo wierzcholkow
    if(ilosc_wierzcholkow < 2) {
        cout << "Graf musi posiadac co najmniej 2 wierzcholki!" << endl;
        return;
    }

    cout << "Podaj gestosc [%]: ";
    cin >> gestosc;

    // Jezeli podalismy niewlasciwa gestosc
    if(gestosc < 0 || gestosc > 100) {
        cout << "Gestosc powinna zawierac sie w przedziale od 0 do 100%." << endl;
        return;
    }

    // Obliczanie maksymalnej liczby krawedzi oraz zadanej liczby krawedzi (gestosc)
    maksymalne_ilosc_krawedzi = ilosc_wierzcholkow * ilosc_wierzcholkow - ilosc_wierzcholkow;
    krawedzie = 0.01 * gestosc * maksymalne_ilosc_krawedzi;

    // Jesli chcemy wygenerowac graf skierowany o zbyt malej gestosci
    if(krawedzie < (ilosc_wierzcholkow - 1) && skierowany) {
        cout << "Graf skierowany o zadanej gestosci nie bedzie spojny. Przerwano generowanie grafu." << endl;
        return;
    }

    // Jesli chcemy wygenerowac graf nieskierowany o zbyt malej gestosci
    if(krawedzie < (2 * (ilosc_wierzcholkow - 1)) && !skierowany) {
        cout << "Graf nieskierowany o zadanej gestosci nie bedzie spojny. Przerwano generowanie grafu." << endl;
        return;
    }

    cout << "Ilosc krawedzi dla tak zadanej gestosci: " << krawedzie << endl;

    // Usuwanie aktualnych danych jezeli jest taka potrzeba
    if(macierzSasiedztwa -> getV() != 0) {
        for(int i = 0; i < macierzSasiedztwa -> getV(); i++) delete[] macierzSasiedztwa -> G[i];
        delete[] macierzSasiedztwa -> G;

        for(int i = 0; i < listySasiadow -> getV(); i++) {
            listySasiadow -> nowy_wierzcholek = listySasiadow -> wierzcholki[i];
            while(listySasiadow -> nowy_wierzcholek) {
                listySasiadow -> usuwany_wierzcholek = listySasiadow -> nowy_wierzcholek;
                listySasiadow -> nowy_wierzcholek = listySasiadow -> nowy_wierzcholek -> getNastepny();
                delete listySasiadow -> usuwany_wierzcholek;
            }
        }

        delete [] listySasiadow -> wierzcholki;
    }

    // Przygotowanie postaci macierzowej
    macierzSasiedztwa -> setV(ilosc_wierzcholkow);
    macierzSasiedztwa -> setE(krawedzie);
    macierzSasiedztwa -> G = new int*[macierzSasiedztwa -> getV()];
    for(int i = 0; i < macierzSasiedztwa -> getV(); i++) macierzSasiedztwa -> G[i] = new int[macierzSasiedztwa -> getV()];

    // Wyzerowanie wszystkich krawedzi
    for(int i = 0; i < macierzSasiedztwa -> getV(); i++) { for(int j = 0; j < macierzSasiedztwa -> getV(); j++) macierzSasiedztwa -> G[i][j] = 0; }

    // Przygotowanie postaci listowej
    listySasiadow -> setV(ilosc_wierzcholkow);
    listySasiadow -> setE(krawedzie);
    listySasiadow -> wierzcholki = new grafListowo::elementListy*[listySasiadow -> getV()];

    // Ustawienie wszystkich polaczen na NULL
    for(int i = 0; i < listySasiadow -> getV(); i++) listySasiadow ->  wierzcholki[i] = NULL;

    // Zmienne pomocnicze do wygenerowania poprawnego grafu (spojnego)
    // Samo to ze graf o zadanej ilosci krawedzi i wierzcholkow bedzie spojny zapewnilismy wyzej instrukcjami warunkowymi
    int spojnosc = 0, wiersz, kolumna;
    bool polaczone[ilosc_wierzcholkow], graf_spojny = false;
    for(int i = 0; i < ilosc_wierzcholkow; i++) polaczone[i] = false;

    // Losowo dobrana waga krawedzi
    int waga;
    if(decyzja) waga = (rand() % 12) - 2;
    else waga = (rand() % 9) + 1;
    // Wierzcholek 0 zawsze bedzie wierzcholkiem startowym
    wiersz = 0;
    listySasiadow -> setPoczatek(0);
    macierzSasiedztwa -> setPoczatek(0);
    // Losowo dobrana kolumna / sasiad
    kolumna = rand() % (ilosc_wierzcholkow - 1) + 1;


    // Ustanowienie polaczenia miedzy wierzcholkami (jednoczesnie w postaci macierzowej i listowej)
    macierzSasiedztwa -> G[wiersz][kolumna] = waga;

    listySasiadow ->  nowy_wierzcholek = new grafListowo::elementListy();
    listySasiadow ->  nowy_wierzcholek -> setWaga(waga);
    listySasiadow ->  nowy_wierzcholek -> setId(kolumna);
    listySasiadow ->  nowy_wierzcholek -> setNastepny(listySasiadow -> wierzcholki[wiersz]);
    listySasiadow ->  wierzcholki[wiersz] = listySasiadow ->  nowy_wierzcholek;

    // Ustawienie indeksow wierzcholkow miedzy ktorymi utworzono polaczenie na "true"
    polaczone[0] = true;
    polaczone[kolumna] = true;

    // W zaleznosci od argumentu funkcji program podejmuje decyzje czy kontynuowac generowanie grafu w sposob
    //      skierowany badz nieskierowany; bedzie on wyznaczal kolejne polaczenia miedzy wierzcholkami
    //      calkowicie losowo do momentu, az otrzymamy graf spojny - reszta zajmuje sie petla nizej
    if(skierowany) {
        while(!graf_spojny) {
            // Resetowanie warunku spojnosci
            spojnosc = 0;
            // Testowanie warunku spojnosci
            for(int i = 0; i < ilosc_wierzcholkow; i++) if(polaczone[i] == true) spojnosc++;
            // Jezeli graf jest spojny to koncz dodawanie krawedzi
            if(spojnosc == ilosc_wierzcholkow) break;
            // Ustanowienie kolejnego wierzcholka wyjsciowego
            wiersz = kolumna;
            // Sprawdzenie czy dolaczymy do naszego podgrafu odizolowany wierzcholek
            while(wiersz == kolumna || polaczone[kolumna] == true) kolumna = rand() % ilosc_wierzcholkow;
            // Losowo dobrana waga krawedzi
            if(decyzja) waga = (rand() % 12) - 2;
            else waga = (rand() % 9) + 1;
            // Ustanowienie krawedzi w postaci macierzowej i listowej
            macierzSasiedztwa -> G[wiersz][kolumna] = waga;
            listySasiadow -> nowy_wierzcholek = new grafListowo::elementListy();
            listySasiadow -> nowy_wierzcholek -> setWaga(waga);
            listySasiadow -> nowy_wierzcholek -> setId(kolumna);
            listySasiadow -> nowy_wierzcholek -> setNastepny(listySasiadow -> wierzcholki[wiersz]);
            listySasiadow -> wierzcholki[wiersz] = listySasiadow -> nowy_wierzcholek;

            // Ustawienie odpowiedniego indeksu odpowiadajacego wierzcholkowi na "true"; wierzcholek nalezy do podgrafu spojnego
            polaczone[kolumna] = true;
        }
    }
    else {
        // Ustanowienie krawedzi w druga strone (od "kolumna" do wierzcholka 0)
        macierzSasiedztwa -> G[kolumna][wiersz] = macierzSasiedztwa -> G[wiersz][kolumna];
        listySasiadow -> nowy_wierzcholek = new grafListowo::elementListy();
        listySasiadow -> nowy_wierzcholek -> setWaga(waga);
        listySasiadow -> nowy_wierzcholek -> setId(wiersz);
        listySasiadow -> nowy_wierzcholek -> setNastepny(listySasiadow -> wierzcholki[kolumna]);
        listySasiadow -> wierzcholki[kolumna] = listySasiadow -> nowy_wierzcholek;

        // Ponizsza petla robi dokladnie to samo co petla while dla grafu skierowanego z tym, ze laczy wierzcholki / sasiadow
        //      w obie strony, poniewaz odpowiada ona grafowi nieskierowanemu; rozbilem to w taki, a nie inny sposob aby latwiej
        //      bylo zobaczyc roznice miedzy tym jak generowany jest graf skierowany i nieskierowany
        while(!graf_spojny) {
            spojnosc = 0;
            for(int i = 0; i < ilosc_wierzcholkow; i++) if(polaczone[i] == true) spojnosc++;

            if(spojnosc == ilosc_wierzcholkow) break;

            wiersz = kolumna;
            while(wiersz == kolumna || polaczone[kolumna] == true) kolumna = rand() % ilosc_wierzcholkow;
            if(decyzja) waga = (rand() % 12) - 2;
            else waga = (rand() % 9) + 1;
            macierzSasiedztwa -> G[wiersz][kolumna] = waga;
            macierzSasiedztwa -> G[kolumna][wiersz] = macierzSasiedztwa -> G[wiersz][kolumna];

            listySasiadow -> nowy_wierzcholek = new grafListowo::elementListy();
            listySasiadow -> nowy_wierzcholek -> setWaga(waga);
            listySasiadow -> nowy_wierzcholek -> setId(kolumna);
            listySasiadow -> nowy_wierzcholek -> setNastepny(listySasiadow -> wierzcholki[wiersz]);
            listySasiadow -> wierzcholki[wiersz] = listySasiadow -> nowy_wierzcholek;

            polaczone[kolumna] = true;

            waga = listySasiadow -> wierzcholki[wiersz] -> getWaga();
            listySasiadow -> nowy_wierzcholek = new grafListowo::elementListy();
            listySasiadow -> nowy_wierzcholek -> setWaga(waga);
            listySasiadow -> nowy_wierzcholek -> setId(wiersz);
            listySasiadow -> nowy_wierzcholek -> setNastepny(listySasiadow -> wierzcholki[kolumna]);
            listySasiadow -> wierzcholki[kolumna] = listySasiadow -> nowy_wierzcholek;
        }
    }

    // Ustawienie wierzcholka koncowego
    listySasiadow -> setKoniec(kolumna);
    macierzSasiedztwa -> setKoniec(kolumna);

    // Tak wygenerowany graf na pewno jest spojny
    graf_spojny = true;

    // Ustalenie ile jeszcze krawedzi mozna dolozyc do grafu (do_konca)
    int do_konca = 0;
    if(!skierowany) do_konca = krawedzie - (2 * (spojnosc - 1));
    else do_konca = krawedzie - spojnosc + 1;

    // Dokladanie kolejnych krawedzi tak dlugo, az spelnimy warunek gestosci
    while(do_konca > 0 && graf_spojny) {
        // Losowanie wiersza i kolumny / sasiada tak dlugo, az bedziemy pewni ze dodamy nowe polaczenie
        while((macierzSasiedztwa -> G[wiersz][kolumna] != 0) || (wiersz == kolumna)) {
            wiersz = rand() % ilosc_wierzcholkow;
            kolumna = rand() % ilosc_wierzcholkow;
        }

        // Losowo dobrana waga krawedzi
        if(decyzja) waga = (rand() % 12) - 2;
        else waga = (rand() % 9) + 1;

        // Ustanowienie krawedzi w postaci macierzowej i listowej
        macierzSasiedztwa -> G[wiersz][kolumna] = waga;
        listySasiadow -> nowy_wierzcholek = new grafListowo::elementListy();
        listySasiadow -> nowy_wierzcholek -> setWaga(waga);
        listySasiadow -> nowy_wierzcholek -> setId(kolumna);
        listySasiadow -> nowy_wierzcholek -> setNastepny(listySasiadow -> wierzcholki[wiersz]);
        listySasiadow -> wierzcholki[wiersz] = listySasiadow -> nowy_wierzcholek;

        // Dodatkowy warunek (krawedz w druga strone) jesli graf jest nieskierowany
        if(!skierowany) {
            macierzSasiedztwa -> G[kolumna][wiersz] = waga;
            listySasiadow -> nowy_wierzcholek = new grafListowo::elementListy();
            listySasiadow -> nowy_wierzcholek -> setWaga(waga);
            listySasiadow -> nowy_wierzcholek -> setId(wiersz);
            listySasiadow -> nowy_wierzcholek -> setNastepny(listySasiadow -> wierzcholki[kolumna]);
            listySasiadow -> wierzcholki[kolumna] = listySasiadow -> nowy_wierzcholek;
        }

        // Zmniejszenie ilosci pozostalych do dodania krawedzi
        if(skierowany) do_konca--;
        else do_konca -= 2;
    }
}
