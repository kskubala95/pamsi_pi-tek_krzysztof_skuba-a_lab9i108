#pragma once
#include "graf.h"
#include <iostream>
#include <cstdlib>
using namespace std;

class Menu {
    private:
        Graf* graf;                                                     // Wskaznik na obiekt typu Graf na ktorym beda przeprowadzane wszystkie operacje
        int wybor_operacji;                                             // Zmienna pomocna przy nawigacji po menu
        bool decyzja;                                                   // Zmienna pomocna przy podejmowaniu decyzji o tym jaki wygenerowac graf
        string nazwa_pliku;                                             // Nazwa wczytywanego z pliku .txt grafu

        void operacje_dla_najkrotszej_sciezki();
        void najkrotsza_sciezka();
    public:
        Menu();
        ~Menu();

        void menu_glowne();
};
