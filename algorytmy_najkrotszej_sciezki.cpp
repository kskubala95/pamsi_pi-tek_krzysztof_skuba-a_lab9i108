#include "graf.h"

void Graf::algorytmDijkstry(bool wybor) {
    if(wybor == 0) listySasiadow -> algorytmDijkstry();
    else macierzSasiedztwa -> algorytmDijkstry();
}

void Graf::grafListowo::algorytmDijkstry() {
    int tab[2][getV()];
    int indeks_najmniejszej_wagi, najmniejsza_waga;
    bool sprawdzone[getV()];

    for(int i = 0; i < getV(); i++) {
        tab[0][i] = INT_MAX;
        tab[1][i] = -1;
    }

    tab[0][getPoczatek()] = 0;

    for(int i = 0; i < getV(); i++) sprawdzone[i] = false;
    sprawdzone[getPoczatek()] = true;

    for(int i = 0; i < getV(); i++) {
        for(int j = 0; j < getV(); j++) if(sprawdzone[j] == true) {
            nowy_wierzcholek = wierzcholki[j];
            while(nowy_wierzcholek) {
                if(nowy_wierzcholek -> getWaga() < 0) {
                    cout << "Moge nie dac sobie rady z ujemnymi wagami... poddaje sie!" << endl;
                    return;
                }
                if(nowy_wierzcholek -> getWaga() + tab[0][j] < tab[0][nowy_wierzcholek -> getId()]) {
                    tab[0][nowy_wierzcholek -> getId()] = nowy_wierzcholek -> getWaga() + tab[0][j];
                    tab[1][nowy_wierzcholek -> getId()] = j;
                }
                nowy_wierzcholek = nowy_wierzcholek -> getNastepny();
            }
        }

        najmniejsza_waga = INT_MAX;
        for(int j = 0; j < getV(); j++) {
            if(najmniejsza_waga > tab[0][j] && sprawdzone[j] == false) {
                najmniejsza_waga = tab[0][j];
                indeks_najmniejszej_wagi = j;
            }
        }
        sprawdzone[indeks_najmniejszej_wagi] = true;
    }

    cout << endl;
    int j, pomoc;
    int temp[getV() - 1];
    for(int i = 0; i < getV(); i++) {
        cout << setw(3) << i << setw(3) << " | " << setw(3) << tab[0][i] << setw(3) << " | ";
        j = i;
        pomoc = 0;
        while(true) {
            temp[pomoc] = j;
            if(tab[1][j] == -1) break;
            j = tab[1][j];
            pomoc++;
        }
        while(true) {
            cout << temp[pomoc];
            pomoc--;
            if(pomoc < 0) break;
            cout << " --> ";
        }
        cout << endl;
    }
    cout << endl;
}

void Graf::grafMacierzowo::algorytmDijkstry() {
    int tab[2][getV()];
    int indeks_najmniejszej_wagi, najmniejsza_waga;
    bool sprawdzone[getV()];

    for(int i = 0; i < getV(); i++) {
        tab[0][i] = INT_MAX;
        tab[1][i] = -1;
    }

    tab[0][getPoczatek()] = 0;

    for(int i = 0; i < getV(); i++) sprawdzone[i] = false;
    sprawdzone[getPoczatek()] = true;

    for(int i = 0; i < getV(); i++) {
        for(int j = 0; j < getV(); j++) if(sprawdzone[j] == true) {
            for(int k = 0; k < getV(); k++) {
                if(G[j][k] < 0) {
                    cout << "Moge nie dac sobie rady z ujemnymi wagami... poddaje sie!" << endl;
                    return;
                }
                if(G[j][k] != 0 && G[j][k] + tab[0][j] < tab[0][k]) {
                    tab[0][k] = G[j][k] + tab[0][j];
                    tab[1][k] = j;
                }
            }
        }

        najmniejsza_waga = INT_MAX;
        for(int j = 0; j < getV(); j++) {
            if(najmniejsza_waga > tab[0][j] && sprawdzone[j] == false) {
                najmniejsza_waga = tab[0][j];
                indeks_najmniejszej_wagi = j;
            }
        }
        sprawdzone[indeks_najmniejszej_wagi] = true;
    }

    cout << endl;
    int j, pomoc;
    int temp[getV() - 1];
    for(int i = 0; i < getV(); i++) {
        cout << setw(3) << i << setw(3) << " | " << setw(3) << tab[0][i] << setw(3) << " | ";
        j = i;
        pomoc = 0;
        while(true) {
            temp[pomoc] = j;
            if(tab[1][j] == -1) break;
            j = tab[1][j];
            pomoc++;
        }
        while(true) {
            cout << temp[pomoc];
            pomoc--;
            if(pomoc < 0) break;
            cout << " --> ";
        }
        cout << endl;
    }
    cout << endl;
}

void Graf::algorytmBellmanaForda(bool wybor) {
    if(wybor == 0) listySasiadow -> algorytmBellmanaForda();
    else macierzSasiedztwa -> algorytmBellmanaForda();
}

void Graf::grafListowo::algorytmBellmanaForda() {
    int tab[2][getV()];
    int indeks_najmniejszej_wagi, najmniejsza_waga;
    bool sprawdzone[getV()];
    bool bez_zmian;

    for(int i = 0; i < getV(); i++) {
        tab[0][i] = INT_MAX;
        tab[1][i] = -1;
    }

    tab[0][getPoczatek()] = 0;

    for(int i = 0; i < getV(); i++) sprawdzone[i] = false;
    sprawdzone[getPoczatek()] = true;

    for(int k = 0; k < getV() - 1; k++) {
        bez_zmian = true;
        for(int i = 0; i < getV(); i++) {
            for(int j = 0; j < getV(); j++) if(sprawdzone[j] == true) {
                nowy_wierzcholek = wierzcholki[j];
                while(nowy_wierzcholek) {
                    if(nowy_wierzcholek -> getWaga() + tab[0][j] < tab[0][nowy_wierzcholek -> getId()]) {
                        tab[0][nowy_wierzcholek -> getId()] = nowy_wierzcholek -> getWaga() + tab[0][j];
                        tab[1][nowy_wierzcholek -> getId()] = j;
                        bez_zmian = false;
                    }
                    nowy_wierzcholek = nowy_wierzcholek -> getNastepny();
                }
            }

            if(bez_zmian) break;

            if(k > 0) {
                for(int c = 0; c < getV(); c++)  {
                    nowy_wierzcholek = wierzcholki[c];
                    while(nowy_wierzcholek) {
                        if(tab[0][nowy_wierzcholek -> getId()] > tab[0][c] + nowy_wierzcholek -> getWaga()) {
                            cout << "Nie dam sobie rady z ujemnym cyklem... poddaje sie!" << endl;
                            return;
                        }
                        nowy_wierzcholek = nowy_wierzcholek -> getNastepny();
                    }
                }
            }

            najmniejsza_waga = INT_MAX;
            for(int j = 0; j < getV(); j++) {
                if(najmniejsza_waga > tab[0][j] && sprawdzone[j] == false) {
                    najmniejsza_waga = tab[0][j];
                    indeks_najmniejszej_wagi = j;
                }
            }
            sprawdzone[indeks_najmniejszej_wagi] = true;
        }
    }

    cout << endl;
    int j, pomoc;
    int temp[getV() - 1];
    for(int i = 0; i < getV(); i++) {
        cout << setw(3) << i << setw(3) << " | " << setw(3) << tab[0][i] << setw(3) << " | ";
        j = i;
        pomoc = 0;
        while(true) {
            temp[pomoc] = j;
            if(tab[1][j] == -1) break;
            j = tab[1][j];
            pomoc++;
        }
        while(true) {
            cout << temp[pomoc];
            pomoc--;
            if(pomoc < 0) break;
            cout << " --> ";
        }
        cout << endl;
    }
    cout << endl;
}

void Graf::grafMacierzowo::algorytmBellmanaForda() {
    int tab[2][getV()];
    int indeks_najmniejszej_wagi, najmniejsza_waga;
    bool sprawdzone[getV()];
    bool bez_zmian;

    for(int i = 0; i < getV(); i++) {
        tab[0][i] = INT_MAX;
        tab[1][i] = -1;
    }

    tab[0][getPoczatek()] = 0;

    for(int i = 0; i < getV(); i++) sprawdzone[i] = false;
    sprawdzone[getPoczatek()] = true;

    for(int k = 0; k < getV() - 1; k++) {
        bez_zmian = true;
        for(int i = 0; i < getV(); i++) {
            for(int j = 0; j < getV(); j++) if(sprawdzone[j] == true) {
                for(int k = 0; k < getV(); k++) {
                    if(G[j][k] != 0 && G[j][k] + tab[0][j] < tab[0][k]) {
                        tab[0][k] = G[j][k] + tab[0][j];
                        tab[1][k] = j;
                        bez_zmian = false;
                    }
                }
            }

            if(bez_zmian) break;

            if(k > 0) {
                for(int c = 0; c < getV(); c++)  {
                    for(int d = 0; d < getV(); d++) {
                        if(tab[0][d] > tab[0][c] + G[c][d]) {
                            cout << "Nie dam sobie rady z ujemnym cyklem... poddaje sie!" << endl;
                            return;
                        }
                    }
                }
            }

            najmniejsza_waga = INT_MAX;
            for(int j = 0; j < getV(); j++) {
                if(najmniejsza_waga > tab[0][j] && sprawdzone[j] == false) {
                    najmniejsza_waga = tab[0][j];
                    indeks_najmniejszej_wagi = j;
                }
            }
            sprawdzone[indeks_najmniejszej_wagi] = true;
        }
    }

    cout << endl;
    int j, pomoc;
    int temp[getV() - 1];
    for(int i = 0; i < getV(); i++) {
        cout << setw(3) << i << setw(3) << " | " << setw(3) << tab[0][i] << setw(3) << " | ";
        j = i;
        pomoc = 0;
        while(true) {
            temp[pomoc] = j;
            if(tab[1][j] == -1) break;
            j = tab[1][j];
            pomoc++;
        }
        while(true) {
            cout << temp[pomoc];
            pomoc--;
            if(pomoc < 0) break;
            cout << " --> ";
        }
        cout << endl;
    }
    cout << endl;
}
