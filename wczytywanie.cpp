#include "graf.h"

// Jednoczesne zaladowanie danych z pliku .txt do postaci macierzowej i listowej
void Graf::wczytaj(string nazwa) {
    listySasiadow -> wczytaj(nazwa);
    macierzSasiedztwa -> wczytaj(nazwa);
}

// Wczytywanie danych do postaci macierzowej
void Graf::grafMacierzowo::wczytaj(string nazwa) {
    ifstream plik;
    nazwa += ".txt";
    plik.open(nazwa);

    if(!plik.good()) {
        cout << "Nie ma takiego pliku." << endl;
        return;
    }

    int licznik = 0, argument;
    string zawartosc_wiersza, pomoc = "";
    getline(plik, zawartosc_wiersza);

    if(getV() != 0) {
        for(int i = 0; i < getV(); i++) delete[] G[i];
        delete[] G;
    }

    for(int i = 0; i < 4; i++) {
        while(zawartosc_wiersza[licznik] < 58 && zawartosc_wiersza[licznik] > 47) {
            pomoc += zawartosc_wiersza[licznik];
            licznik++;
        }

        argument = atoi(pomoc.c_str());
        if(i == 0) { setE(argument); }
        if(i == 1) { setV(argument); }
        if(i == 2) { setPoczatek(argument); }
        if(i == 3) { setKoniec(argument); }

        licznik++;
        pomoc = "";
    }

    G = new int*[getV()];
    for(int i = 0; i < getV(); i++) G[i] = new int[getV()];
    for(int i = 0; i < getV(); i++) { for(int j = 0; j < getV(); j++) G[i][j] = 0; }

    licznik = 0;
    int masymalna_ilosc_krawedzi = getV() * getV() - getV();
    int wiersz, kolumna, waga;
    for(int i = 0; i < masymalna_ilosc_krawedzi; i++) {
        getline(plik, zawartosc_wiersza);
        if(zawartosc_wiersza[0] > 57 || zawartosc_wiersza[0] < 48 || i == getE()) break;

        for(int j = 0; j < 3; j++) {
            while(zawartosc_wiersza[licznik] < 58 && zawartosc_wiersza[licznik] > 47 || zawartosc_wiersza[licznik] == 45) {
                pomoc += zawartosc_wiersza[licznik];
                licznik++;
            }

            argument = atoi(pomoc.c_str());
            if(j == 0) wiersz = argument;
            if(j == 1) kolumna = argument;
            if(j == 2) waga = argument;

            licznik++;
            pomoc = "";
        }

        licznik = 0;
        G[wiersz][kolumna] = waga;
    }

    plik.close();
}

// Wczytywanie danych do postaci listowej
void Graf::grafListowo::wczytaj(string nazwa) {
    ifstream plik;
    nazwa += ".txt";
    plik.open(nazwa);

    if(!plik.good()) {
        cout << "Nie ma takiego pliku." << endl;
        return;
    }

    int licznik = 0, argument;
    string zawartosc_wiersza, pomoc = "";
    getline(plik, zawartosc_wiersza);

    if(getV() != 0) {
        for(int i = 0; i < getV(); i++) {
            nowy_wierzcholek = wierzcholki[i];
            while(nowy_wierzcholek) {
                usuwany_wierzcholek = nowy_wierzcholek;
                nowy_wierzcholek = nowy_wierzcholek -> getNastepny();
                delete usuwany_wierzcholek;
            }
        }

        delete [] wierzcholki;
    }

    for(int i = 0; i < 4; i++) {
        while(zawartosc_wiersza[licznik] < 58 && zawartosc_wiersza[licznik] > 47) {
            pomoc += zawartosc_wiersza[licznik];
            licznik++;
        }

        argument = atoi(pomoc.c_str());
        if(i == 0) { setE(argument); }
        if(i == 1) { setV(argument); }
        if(i == 2) { setPoczatek(argument); }
        if(i == 3) { setKoniec(argument); }

        licznik++;
        pomoc = "";
    }

    wierzcholki = new elementListy*[getV()];
    for(int i = 0; i < getV(); i++) wierzcholki[i] = NULL;

    licznik = 0;
    int masymalna_ilosc_krawedzi = getV() * getV() - getV();
    int sasiad, numer_wierzcholka, waga;
    for(int i = 0; i < masymalna_ilosc_krawedzi; i++) {
        getline(plik, zawartosc_wiersza);
        if(zawartosc_wiersza[0] > 57 || zawartosc_wiersza[0] < 48 || i == getE()) break;

        for(int j = 0; j < 3; j++) {
            while(zawartosc_wiersza[licznik] < 58 && zawartosc_wiersza[licznik] > 47 || zawartosc_wiersza[licznik] == 45) {
                pomoc += zawartosc_wiersza[licznik];
                licznik++;
            }

            argument = atoi(pomoc.c_str());
            if(j == 0) sasiad = argument;
            if(j == 1) numer_wierzcholka = argument;
            if(j == 2) waga = argument;

            licznik++;
            pomoc = "";
        }

        licznik = 0;
        nowy_wierzcholek = new elementListy();
        nowy_wierzcholek -> setWaga(waga);
        nowy_wierzcholek -> setId(numer_wierzcholka);
        nowy_wierzcholek -> setNastepny(wierzcholki[sasiad]);
        wierzcholki[sasiad] = nowy_wierzcholek;
    }

    plik.close();
}
